/* je wilt de oneven getallen verdubbelen, maar de oneven negeren.
Dat kan je met devolgde constructie:
*/
let numberList = [1, 2, 3, 4, 5, 6];
let newNumberList = numberList.filter(function(number) {
    return (number % 2 !== 0);
}).map(function(number) {
    return number * 2;
});
console.log("The doubled numbers are", newNumberList); // [2, 6, 12]