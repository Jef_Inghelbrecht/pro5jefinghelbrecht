let persoon = [];
persoon['Voornaam'] = 'Jan';
persoon['Familienaam'] = 'Hoet';


let adres = 'Potstraat 23';
let persoon1 = {
    Voornaam: 'Louis',
    Familienaam: 'Achterhuis',
    IkBen: function () { alert(this.Voornaam + this.Familienaam) },
    adres,
    name() { return this.Stad }
}
document.
    querySelector('button').
    addEventListener('click', function () {
        alert(persoon1.name())
    });

document.
    querySelector('button#persoon2').
    addEventListener('click', function () {
        persoon2.setFirstName = 'Abdel';
        alert(persoon2['Voornaam']);
        alert(persoon2.Voornaam)
    });

document.
    querySelector('button#persoon3').
    addEventListener('click', function () {
        persoon3.Zeg();
    });

persoon1["Stad"] = 'Antwerpen';

let setFirstName = 'setFirstName';
let getFirstName = 'getFirstName';
let persoon2 = {
    Voornaam: 'An',
    set [setFirstName](value) { this.Voornaam = value },
    get [getFirstName]() { return this.Voornaam }
}

let persoon3 = new Object;
persoon3['Hallo'] = function () { alert('Hallo'); };
persoon3.Voornaam = 'Hannes';
persoon3.Zeg = function () { alert(this.Voornaam) };

function change(js_object)
{
    js_object.Voornaam = 'Katrien';
    return js_object;
}

// persoon4 = new persoon3;

change(persoon3);
// examenvraag: is persoon3 gewijzigd?
// staat er nu Katrien in of is het nog altijd Hannes?
alert(`Objecten zijn by reference: ${persoon3.Voornaam}`);

// examenvraag: wat is de voornaam van persoon4?
// alert(`Objecten zijn by reference: ${persoon4.Voornaam}`);

function Boek() {
    this.voornaam;
    this.familienaam;
    this.titel
};

Boek.prototype.Boeken = [];

let boek1 = new Boek();
boek1.voornaam = 'Jan';
boek1.familienaam = 'Jannsens';
boek1.titel = 'Een goed boek';

typeof boek1;