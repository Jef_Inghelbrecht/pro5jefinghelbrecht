const data = `{
    "type": "FeatureCollection",
    "features": [
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4526,
          "NAAM": "Bibliotheek Bist",
          "STRAATNAAM": "Bist",
          "HUISNUMMER": "1",
          "POSTCODE": "2610",
          "DISTRICT": "Wilrijk",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; WO en DO van 10-20u. VR en ZA van 10-17u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00001"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.394278224764265,
            51.16936913505678
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4527,
          "NAAM": "Webpunt Luchtbal",
          "STRAATNAAM": "Columbiastraat",
          "HUISNUMMER": "110",
          "POSTCODE": "2030",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": "DI (10u-12u45 - 13u30-16u45) - WO (13u30-19u45) - DO (13u30 -16u45)",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00002"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.4234336406417905,
            51.25298085471671
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4528,
          "NAAM": "Bibliotheek Luchtbal",
          "STRAATNAAM": "Columbiastraat",
          "HUISNUMMER": "110",
          "POSTCODE": "2030",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI en DO van 10-18u. WO van 10-20u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00003"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.4234336406417905,
            51.25298085471671
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4529,
          "NAAM": "Bibliotheek Permeke",
          "STRAATNAAM": "De Coninckplein",
          "HUISNUMMER": "26",
          "POSTCODE": "2060",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "MA; DI; WO en DO van 10-20u. VR en ZA van 10-17uur. ZO van 10-14u.",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00005"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.421563046186537,
            51.22183823737212
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4530,
          "NAAM": "Webpunt Permeke",
          "STRAATNAAM": "De Coninckplein",
          "HUISNUMMER": "26",
          "POSTCODE": "2060",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 641 08 63",
          "OPENINGSUREN": " ",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van Atel",
          "WEBSITE": null,
          "GISID": "WEBPUNT00006"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.421563046186537,
            51.22183823737212
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4531,
          "NAAM": "Bibliotheek Arena",
          "STRAATNAAM": "Arenaplein",
          "HUISNUMMER": "62A",
          "POSTCODE": "2100",
          "DISTRICT": "Deurne",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "MA van 9-12u DI en DO van 10-18u WO van 10-20u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00008"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.45956803460719,
            51.20660702338607
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4532,
          "NAAM": "Bibliotheek Driehoek",
          "STRAATNAAM": "Driehoekstraat",
          "HUISNUMMER": "43",
          "POSTCODE": "2180",
          "DISTRICT": "Ekeren",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; WO en DO van 10-20u. VR en ZA van 10-17u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00009"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.420052655523335,
            51.28088968547718
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4533,
          "NAAM": "Antwerpen.be-centrum",
          "STRAATNAAM": "Generaal Armstrongweg",
          "HUISNUMMER": "1",
          "POSTCODE": "2020",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 338 99 41",
          "OPENINGSUREN": "MA; WO; DO; VR van 9-16.30u.; DI van 9-21.00u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van antwerpen.be-centrum",
          "WEBSITE": null,
          "GISID": "WEBPUNT00010"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.380871197658372,
            51.19477327840001
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4534,
          "NAAM": "Bibliotheek UiThuis",
          "STRAATNAAM": "Kapelstraat",
          "HUISNUMMER": "64",
          "POSTCODE": "2660",
          "DISTRICT": "Hoboken",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; WO en DO van 10-20u. VR en ZA van 10-17u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00011"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.347327206372894,
            51.17524547758454
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4535,
          "NAAM": "Webpunt Posthof",
          "STRAATNAAM": "Patriottenstraat",
          "HUISNUMMER": "62",
          "POSTCODE": "2600",
          "DISTRICT": "Berchem",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": " ",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00020"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.423483741939772,
            51.195011312525494
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4536,
          "NAAM": "Nova Ontmoetingscentrum",
          "STRAATNAAM": "Schijfstraat",
          "HUISNUMMER": "105",
          "POSTCODE": "2020",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": "Di - vr van 9-17u",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00021"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.373077338396452,
            51.19068355500966
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4537,
          "NAAM": "Webpunt 't Pleintje",
          "STRAATNAAM": "Sint-Rochusstraat",
          "HUISNUMMER": "106",
          "POSTCODE": "2100",
          "DISTRICT": "Deurne",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": " ",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00022"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.463949350556624,
            51.204640502193534
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4538,
          "NAAM": "Bibliotheek Couwelaar",
          "STRAATNAAM": "Te Couwelaarlei",
          "HUISNUMMER": "120",
          "POSTCODE": "2100",
          "DISTRICT": "Deurne",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; WO en DO van 10-20u. VR en ZA van 10-17u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00023"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.458291786895119,
            51.222885409956085
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4539,
          "NAAM": "Bibliotheek Viswater",
          "STRAATNAAM": "Tongerlostraat",
          "HUISNUMMER": "2",
          "POSTCODE": "2040",
          "DISTRICT": "Berendrecht-Zandvliet-Lillo",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; DO van 10-18u. WO van 10-20u. Iedere 2e en 4e ZA v/d maand van 10-14u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00025"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.330403037244435,
            51.3470679152972
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4540,
          "NAAM": "Bibliotheek Park",
          "STRAATNAAM": "Van Heybeeckstraat",
          "HUISNUMMER": "28A",
          "POSTCODE": "2170",
          "DISTRICT": "Merksem",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; WO en DO van 10-20u. VR en ZA van 10-17u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00028"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.449169766686986,
            51.247079368501744
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4541,
          "NAAM": "Webpunt Linkeroever",
          "STRAATNAAM": "Willem Elsschotstraat",
          "HUISNUMMER": "5",
          "POSTCODE": "2050",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": "DI (10u-12u45 - 13u30-16u45) - WO (13u30-19u45) - DO (13u30-16u45)",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00031"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.380412109069372,
            51.22623308180671
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4542,
          "NAAM": "Bibliotheek De Poort",
          "STRAATNAAM": "Willem Van Laarstraat",
          "HUISNUMMER": "15-17",
          "POSTCODE": "2600",
          "DISTRICT": "Berchem",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI; WO en DO van 10-20u. VR en ZA van 10-17u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00033"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.420850548576837,
            51.19563904475447
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4543,
          "NAAM": "Bibliotheek Kielpark",
          "STRAATNAAM": "Wittestraat",
          "HUISNUMMER": "10",
          "POSTCODE": "2020",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI en DO van 10-18u. WO van 10-20u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00034"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.386092790464998,
            51.18974275863792
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4544,
          "NAAM": "Webpunt Atlas",
          "STRAATNAAM": "Carnotstraat",
          "HUISNUMMER": "110",
          "POSTCODE": "2060",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": " ",
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00035"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.426488690289837,
            51.21748908925351
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4545,
          "NAAM": "Webpunt Couwelaar",
          "STRAATNAAM": "Te Couwelaarlei",
          "HUISNUMMER": "120",
          "POSTCODE": "2100",
          "DISTRICT": "Deurne",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": "WO (13u30-16u45) - DO (12u-19u45)",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00037"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.457903518153221,
            51.222741341344566
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4546,
          "NAAM": "Webpunt Hoboken",
          "STRAATNAAM": "Kapelstraat",
          "HUISNUMMER": "64",
          "POSTCODE": "2660",
          "DISTRICT": "Hoboken",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": "DI (13u30-16u45) - WO (10u-12u45 - 13u30-16u45) - DO (12u-19u45)",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00038"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.34737449459876,
            51.175095906601115
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4547,
          "NAAM": "Webpunt Merksem",
          "STRAATNAAM": "Minister Schollaertstraat",
          "HUISNUMMER": "24",
          "POSTCODE": "2170",
          "DISTRICT": "Merksem",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 286 85 85",
          "OPENINGSUREN": "DI en WO (9u-12u45 - 13u30-16u45)  - DO (12u-19u45)",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van dot.kom",
          "WEBSITE": null,
          "GISID": "WEBPUNT00039"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.44381131660028,
            51.23942042322337
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4548,
          "NAAM": "Bibliotheek Vredegerecht",
          "STRAATNAAM": "Moorkensplein",
          "HUISNUMMER": "1",
          "POSTCODE": "2140",
          "DISTRICT": "Borgerhout",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI en DO van 10-18u. WO van 10-20u.",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00026"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.433512410573726,
            51.21359070509739
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4549,
          "NAAM": "Bibliotheek Elsschot",
          "STRAATNAAM": "Willem Elsschotstraat",
          "HUISNUMMER": "5",
          "POSTCODE": "2050",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt zonder begeleiding",
          "BEGELEIDING": "nee",
          "TELEFOON": "03 22 11 333",
          "OPENINGSUREN": "DI en DO van 10-18u. WO van 10-20u",
          "KOSTPRIJS": "gratis",
          "BEGINDATUM": null,
          "OPMERKING": "Bibliotheek met computer",
          "WEBSITE": null,
          "GISID": "WEBPUNT00032"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.380412098971795,
            51.22629221717569
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "OBJECTID": 4550,
          "NAAM": "Webpunt Costa",
          "STRAATNAAM": "Sint-Andriesplaats",
          "HUISNUMMER": "25",
          "POSTCODE": "2000",
          "DISTRICT": "Antwerpen",
          "TYPE": "Webpunt met begeleiding en computercursussen",
          "BEGELEIDING": "ja",
          "TELEFOON": "03 641 08 63",
          "OPENINGSUREN": null,
          "KOSTPRIJS": " ",
          "BEGINDATUM": null,
          "OPMERKING": "Webpunt van Atel",
          "WEBSITE": null,
          "GISID": "WEBPUNT00014"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [
            4.396053469257631,
            51.21495826303168
          ]
        }
      }
    ]
  }`;