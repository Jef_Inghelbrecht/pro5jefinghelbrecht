'use strict';
const e = React.createElement;
class MarvelCharacters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        };
    }
    loaded(response) {
        this.setState({ list: response, loaded: true })
    }
    render() {
        if (this.state.loaded) {
            return React.createElement(
                'pre',
                { 'id': 'response' },
                 this.state.list
            );
        }
        return e(
            'div', {id: "marvel"},
                e('button',
                { onClick: () => loadJSON('data/marvel-top-characters.json', response => this.loaded(response)) },
                'Toon Marvel personnages in JSON tekst'),
                e('h2', {id : "title"}, this.props.title)
            );
    }
}

