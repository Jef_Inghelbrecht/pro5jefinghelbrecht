function loadJSON(fileName, callback) {
    let ajax = new XMLHttpRequest();
    ajax.overrideMimeType("application/json");
    ajax.open('GET', fileName, true); // Replace 'appDataServices' with the path to your file
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4 && ajax.status == "200") {
            // Required use of an anonymous callback as .open will 
            // NOT return a value but simply returns undefined in asynchronous mode
            callback(this.responseText);
        }
    };
    ajax.send(null);
}
