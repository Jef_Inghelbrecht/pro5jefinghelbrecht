var loadMarvelCharacters = function (that) {
    var ajax;
    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
    }
    else {
        // code for older browsers
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var me = this;
    // callback functie
    ajax.onreadystatechange = function () {
        // server sent response and
        // OK 200 The request was fulfilled.
        if (ajax.readyState == 4 && ajax.status == 200) {
            // we maken een tabel element
            showMarvelCharacters(this.responseText);
        }
    };
    ajax.open("GET", "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=5e50ffa08294f9673a4876bb8738ab43", true);
    ajax.send();
}

function showMarvelCharacters(response) {
    // convert JSON to array
    // we zijn alleen geïnteresseerd in de personnages
    var characters = JSON.parse(response).data.results;
    var table = document.createElement('table');
    characters.map(function (character) {
        var row = document.createElement('tr');
        var name = document.createElement('td');
        var textContent = document.createTextNode(character.name);
        name.appendChild(textContent);
        row.appendChild(name);
        table.appendChild(row);
    });
    document.body.appendChild(table);
    document.getElementById('pre').innerHTML = response;
}

function loadHtmlPage() {
    var ajax;
    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
    } else {
        // code for older browsers
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // callback functie
    ajax.onreadystatechange = function () {
        // server sent response and
        // OK 200 The request was fulfilled.
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("demo").innerHTML =
                this.responseText;
        }
    };
    ajax.open("GET", "https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=spider&apikey=5e50ffa08294f9673a4876bb8738ab43", true);
    ajax.send();
}

loadHtmlPage();

function getMarvelCharacters() {
    var ajax;
    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
    } else {
        // code for older browsers
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // callback functie
    ajax.onreadystatechange = function () {
        // server sent response and
        // OK 200 The request was fulfilled.
        if (this.readyState == 4 && this.status == 200) {
            createHTML(this.responseText);
        } else if (this.readyState == 3) {
            // alert('Een beetje geduld! Data komt eraan.');
        }
    };
    ajax.open("GET", "https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=spider&apikey=5e50ffa08294f9673a4876bb8738ab43", true);
    ajax.send();
}

function createHTML(response) {
    // document.getElementById("demo").innerHTML =
    // response;
    var characters = JSON.parse(response).data;
    // alert(JSON.stringify(characters.results[0]));
    for (let i = 0; i <= characters.count - 5; i++) {
        let figureElem = document.createElement('FIGURE');
        let image = document.createElement('IMG');
        image.src = characters.results[i].thumbnail.path + '/standard_fantastic.' + characters.results[i].thumbnail.extension;
        image.className = 'rotate-in';
        figureElem.appendChild(image);
        let figCaptionElem = document.createElement('FIGCAPTION');
        let textElem = document.createTextNode(characters.results[i].name);
        figCaptionElem.appendChild(textElem);
        figureElem.appendChild(figCaptionElem);
        document.querySelector('article').appendChild(figureElem)
    }
}
getMarvelCharacters();